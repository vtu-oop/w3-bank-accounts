using BankAccounts.Interfaces;

namespace BankAccounts.Models;

public class Deposit : Account, IDepositable, IWithdrawable
{
    public override decimal CalculateInterestValue(int months)
    {
        if (Balance > 0 && Balance < 1000)
        {
            return 0;
        }

        return base.CalculateInterestValue(months);
    }

    public void DepositMoney()
    {
        throw new NotImplementedException();
    }

    public void WithdrawMoney()
    {
        throw new NotImplementedException();
    }
}