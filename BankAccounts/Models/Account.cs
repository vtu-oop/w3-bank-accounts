namespace BankAccounts.Models;

public abstract class Account
{
    public Customer Customer { get; set; }
    public decimal Balance { get; set; }
    public decimal InterestRate { get; set; }

    public virtual decimal CalculateInterestValue(int months)
    {
        return months * InterestRate;
    }
}