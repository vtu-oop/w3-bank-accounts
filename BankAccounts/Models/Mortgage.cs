using BankAccounts.Interfaces;

namespace BankAccounts.Models;

public class Mortgage : Account, IDepositable
{
    public override decimal CalculateInterestValue(int months)
    {
        if (Customer is BusinessCustomer)
        {
            if (months <= 12)
            {
                return base.CalculateInterestValue(months) / 2;
            }
            else
            {
                decimal interestValue = 12 * InterestRate / 2;
                months -= 12;
                interestValue += months * InterestRate;

                return interestValue;
            }
        }

        if (Customer is IndividualCustomer)
        {
            months = months - 6 < 0 ? 0 : months - 6;
            return base.CalculateInterestValue(months);
        }

        return base.CalculateInterestValue(months);
    }

    public void DepositMoney()
    {
        throw new NotImplementedException();
    }
}