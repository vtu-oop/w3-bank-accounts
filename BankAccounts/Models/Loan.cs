using BankAccounts.Interfaces;

namespace BankAccounts.Models;

public class Loan : Account, IDepositable
{
    public override decimal CalculateInterestValue(int months)
    {
        if (Customer is IndividualCustomer)
        {
            months = months - 3 < 0 ? 0 : months - 3;
        }

        if (Customer is BusinessCustomer)
        {
            months = months - 2 < 0 ? 0 : months - 2;
        }

        return base.CalculateInterestValue(months);
    }

    public void DepositMoney()
    {
        throw new NotImplementedException();
    }
}