namespace BankAccounts.Interfaces;

public interface IWithdrawable
{
    void WithdrawMoney();
}