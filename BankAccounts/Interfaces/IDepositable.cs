namespace BankAccounts.Interfaces;

public interface IDepositable
{
    void DepositMoney();
}